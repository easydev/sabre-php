<?php
namespace Easydev\Vendor\Sabredev\Client\Rest\Methods\shop;

use Easydev\Vendor\Sabredev\Client\Exceptions\InvalidParameterException;
use Easydev\Vendor\Sabredev\Client\SabredevRequest;

/**
 * @property $origin
 * @property $destination
 * @property $departuredate
 * @property $returndate
 * @property $includedcarriers
 * @property $excludedcarriers
 * @property $outboundflightstops
 * @property $inboundflightstops
 * @property $includedconnectpoints
 */
class FlightsRequest extends SabredevRequest
{
    const API_ENDPOINT = '/shop/flights';
    const API_METHOD = 'GET';

    public static $parameters = [
        'origin' => true,
        'destination' => true,
        'departuredate' => true,
        'returndate' => true,
        'includedcarriers' => false,
        'excludedcarriers' => false,
        'outboundflightstops' => false,
        'inboundflightstops' => false,
        'includedconnectpoints' => false,
        'excludedconnectpoints' => false,
        'outboundstopduration' => false,
        'inboundstopduration' => false,
    ];

    public function __get($name)
    {
        
    }

    public function __set($name, $value)
    {
        if(array_key_exists($name,self::$parameters)){
            $this->$name = $value;
        } else {
            throw new InvalidParameterException;
        }
    }
}
