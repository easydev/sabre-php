<?php
namespace Easydev\Vendor\Sabredev\Client\Rest;

use Easydev\Vendor\Sabredev\Client\Client as BaseClient;
use Easydev\Vendor\Sabredev\Client\SabredevRequest;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Post\PostBody;

class Client implements BaseClient
{
    const BASE_URL = "https://api.test.sabre.com/";
    const API_VERSION = "v1";
    const AUTH_ENDPOINT = 'auth/token';
    const API_TYPE = "REST";

    public $access_token;

    public function __construct(RestCredentials $credentials, HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->credentials = $credentials;
    }

    public function authenticate()
    {
        $request = new Request('POST', self::BASE_URL . self::API_VERSION . '/' . self::AUTH_ENDPOINT);
        $request->setHeader('Authorization', 'Basic ' . $this->credentials->getApiRequestCredentials());
        $request->setHeader('Content-Type', 'application/x-www-form-urlencoded');
        $postBody = new PostBody();
        $postBody->setField('grant_type', 'client_credentials');
        $request->setBody($postBody);
        $tokenResponse = $this->httpClient->send($request);

        $this->populateFromResponse($tokenResponse);
    }

    public function isAuthenticated()
    {
        return !is_null($this->access_token);
    }

    public function getAccessToken()
    {
        return $this->access_token;
    }

    public function getTokenExpiryDate()
    {
        return $this->created_at + $this->expires_in;
    }

    private function populateFromResponse(Response $tokenResponse)
    {
        $repsonseArray = $tokenResponse->json();
        $this->access_token = $repsonseArray['access_token'];
        $this->token_type = $repsonseArray['token_type'];
        $this->expires_in = $repsonseArray['expires_in'];
        $this->created_at = time();
    }

    /**
     * @param SabredevRequest $apiRequest
     * @return \GuzzleHttp\Message\FutureResponse|\GuzzleHttp\Message\ResponseInterface|\GuzzleHttp\Ring\Future\FutureInterface|null
     */
    public function executeRequest(SabredevRequest $apiRequest)
    {
        $request = new Request($apiRequest::API_METHOD, $this->getRequestUrl($apiRequest));
        $request->setHeader('Authorization', 'Bearer ' . $this->getAccessToken());
//        $request->setHeader('Content-Type', 'application/json');
//        $postBody = new PostBody();
//        while (list($parameter, $value) = each($apiRequest)) {
//            $postBody->setField($parameter, $value);
//        }
//        $request->setBody($postBody);
        $response = $this->httpClient->send($request);

        return $response->json();
    }

    private function getRequestUrl(SabredevRequest $apiRequest)
    {
        if($apiRequest::API_METHOD == 'GET'){
            $url  = self::BASE_URL;
            $url .= self::API_VERSION;
            $url .= $apiRequest::API_ENDPOINT;
            $url .= '?'.http_build_query((array)$apiRequest);
            return $url;
        }
    }
}
