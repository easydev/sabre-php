<?php
namespace Easydev\Vendor\Sabredev\Client\Rest;

class RestCredentials
{
    const API_TYPE = "REST";

    public function __construct($clientId, $clientSecret)
    {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    public function getApiRequestCredentials()
    {
        return base64_encode(base64_encode($this->clientId).':'.base64_encode($this->clientSecret));
    }
}
