<?php
namespace Easydev\Vendor\Sabredev\Client;

interface Client
{
    /**
     * @return mixed
     */
    public function authenticate();

    /**
     * @return mixed
     */
    public function isAuthenticated();

    /**
     * @param SabredevRequest $request
     * @return mixed
     */
    public function executeRequest(SabredevRequest $request);
}