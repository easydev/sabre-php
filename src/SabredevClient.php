<?php

namespace Easydev\Vendor\Sabredev;

use Easydev\Vendor\Sabredev\Client\Rest\Client;
use Easydev\Vendor\Sabredev\Client\Rest\RestCredentials;
use Easydev\Vendor\Sabredev\Client\SabredevRequest;

class SabredevClient
{
    public function __construct(RestCredentials $credentials)
    {
        $this->credentials = $credentials;
    }

    public function test()
    {
        $client = new Client($this->credentials, new \GuzzleHttp\Client());
        $client->authenticate();
    }

    /**
     * @param SabredevRequest $request
     */
    public function executeRequest(SabredevRequest $request)
    {
        $client = new Client($this->credentials, new \GuzzleHttp\Client());
        if(!$client->isAuthenticated()){
            $client->authenticate();
        }

        $response = $client->executeRequest($request);
    }

}
