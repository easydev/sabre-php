<?php
namespace tests\integration;

use Easydev\Vendor\Sabredev\Client\Rest\Methods\shop\FlightsRequest;
use Easydev\Vendor\Sabredev\SabredevClient;
use Easydev\Vendor\Sabredev\Client\Rest\RestCredentials;

class SabredevRestClientTest extends \PHPUnit_Framework_TestCase
{
    const EASYDEV_CLIENT_SECRET = 'Bm6WCm1a';
    const EASYDEV_CLIENT_ID = 'V1:4lyd4cdmt2pzhww1:DEVCENTER:EXT';

    /**
     * @var SabredevClient
     */
    protected $client;

    public function setUp()
    {
        $this->restClient = $this->createSabredevRestClient();
    }

    public function testAuthentication()
    {
        $this->client->test();
    }

    /**
     * @return SabredevClient
     * @internal param string $organisationId
     * @internal param string $apiKey
     * @internal param string $baseUrl
     */
    protected function createSabredevRestClient()
    {
        $restCredentials = new RestCredentials(self::EASYDEV_CLIENT_ID, self::EASYDEV_CLIENT_SECRET);
        return new SabredevClient($restCredentials);
    }

    public function testFlightSearchRequest()
    {
        $flightSearchRequest = new FlightsRequest();
        $flightSearchRequest->destination = 'JFK';
    }

    /**
     * @expectedException Easydev\Vendor\Sabredev\Client\Exceptions\InvalidParameterException
     */
    public function testFlightSearchThrowsIfParameterDoesntExist()
    {
        $flightSearchRequest = new FlightsRequest();
        $flightSearchRequest->flightSearchWOW = 'JFK';
    }

    public function testExecuteReqest()
    {
        $departureDate = (new \DateTime())->add(new \DateInterval('P1D'));
        $returnDate = (new \DateTime())->add(new \DateInterval('P5D'));
        $flightSearchRequest = new FlightsRequest();
        $flightSearchRequest->origin = 'POZ';
        $flightSearchRequest->destination = 'LTN';
        $flightSearchRequest->departuredate = $departureDate->format('Y-m-d');
        $flightSearchRequest->returndate = $returnDate->format('Y-m-d');
        $this->restClient->executeRequest($flightSearchRequest);
    }
}
