<?php

namespace spec\Easydev\Vendor\Sabredev\Client\Rest;

use Easydev\Vendor\Sabredev\Client\Rest\RestCredentials;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RestCredentialsSpec extends ObjectBehavior
{
    const MOCK_CLIENT_ID = 'mock_client_id';
    const MOCK_CLIENT_SECRET = 'mock_client_secret';

    function let()
    {
        $this->beAnInstanceOf(RestCredentials::class);
        $this->beConstructedWith(self::MOCK_CLIENT_ID, self::MOCK_CLIENT_SECRET);
    }
}
