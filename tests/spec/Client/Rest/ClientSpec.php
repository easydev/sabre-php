<?php

namespace spec\Easydev\Vendor\Sabredev\Client\Rest;

use Easydev\Vendor\Sabredev\Client\Rest\Client;
use Easydev\Vendor\Sabredev\Client\Rest\RestCredentials;
use GuzzleHttp\Message\Response;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument as a;
use GuzzleHttp\Client as HttpClient;

/**
 * @mixin Client
 */
class ClientSpec extends ObjectBehavior
{
    /** @var HttpClient */
    protected $httpClient;
    /** @var  RestCredentials */
    protected $restCredentials;

    function let(RestCredentials $restCredentials, HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->restCredentials = $restCredentials;

        $this->beAnInstanceOf(Client::class);
        $this->beConstructedWith($this->restCredentials, $this->httpClient);
    }

    function it_calls_http_client_with_request_object(Response $response)
    {
        $this->httpClient->send(a::any())
            ->shouldBeCalled()
            ->willReturn($response);

        $this->authenticate();
    }

    function it_populates_object_data_from_authentication_response()
    {

    }
}
