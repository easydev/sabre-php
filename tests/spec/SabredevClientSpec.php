<?php

namespace spec\Easydev\Vendor\Sabredev;

use Easydev\Vendor\Sabredev\SabredevClient;
use Easydev\Vendor\Sabredev\Client\Rest\RestCredentials;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SabredevClientSpec extends ObjectBehavior
{
    function let(
        RestCredentials $credentials
    )
    {
        $this->beAnInstanceOf(SabredevClient::class);
        $this->beConstructedWith($credentials);
    }
}
